/*
 * nrf.c
 *
 *  Created on: 19.09.2017
 *      Author: annetta897
 */
#include <nrf.h>
#include <spi.h>
#include <avr/io.h>

#define CE_PORT PORTB
#define CE_DDR DDRB
#define CE_BIT 0

#ifndef RADIO_CS
	#define RADIO_CS 6
#endif

const NRF_config_t tx_config[] = {
		{CONFIG, 0x38},
		{SETUP_RETR, 0xF3},
		{RF_CH, RF_CH_VAL},
		{RX_PW_P(0), PAYLOAD_LENGTH},
		{RF_SETUP, 0x26},
		{CONFIG, 0x3A}
};

const NRF_config_t rx_config[] = {
		{CONFIG, 0x39},
		{SETUP_RETR, 0xF3},
		{RF_CH, RF_CH_VAL},
		{RX_PW_P(0), PAYLOAD_LENGTH},
		{RF_SETUP, 0x26},
		{CONFIG, 0x3B}
};


void RadioPortsConfig (void)
{
	SPIInit();
	CE_DDR |= (1<<CE_BIT);
	CE_PORT |= (1<<CE_BIT);
}

void RadioConfig(mode_t mode)
{
	uint8_t i;
	RadioPortsConfig();
	if (mode == receive)
		for (i = 0; i < sizeof(rx_config)/sizeof(NRF_config_t); i++)
			RadioSetRegVal (rx_config[i].addr, rx_config[i].data);
	else
		for (i = 0; i < sizeof(tx_config)/sizeof(NRF_config_t); i++)
			RadioSetRegVal (tx_config[i].addr, tx_config[i].data);

}

uint8_t RadioSendCmd (uint8_t cmd, uint8_t * data_w, uint8_t * data_r, uint8_t count)
{
	uint8_t i, spi_data[33];
	spi_data[0] = cmd;
	if (data_w)
		for (i = 0; i < count; i++)
			spi_data[i+1] = data_w[i];
	SPISend (spi_data, spi_data, count+1, RADIO_CS);
	if (data_r)
		for (i = 0; i < count; i++)
			data_r[i] = spi_data[i+1];
	return spi_data[0];
}

uint8_t RadioGetStatus (void)
{
	uint8_t status = NOP;
	SPISend (&status, &status, 1, RADIO_CS);
	return status;
}

uint8_t RadioSetRegVal (uint8_t addr, uint8_t data)
{
	return RadioSendCmd (W_REGISTER+addr, &data, 0,  1);
}

uint8_t RadioGetRegVal (uint8_t addr)
{
	uint8_t data;
	RadioSendCmd (R_REGISTER+addr, 0, &data, 1);
	return data;
}

void RadioWriteData (uint8_t * data)
{
	RadioSendCmd (W_TX_PAYLOAD, data, 0, PAYLOAD_LENGTH);
}

void RadioReadData (uint8_t * data)
{
	RadioSendCmd (R_RX_PAYLOAD, 0, data, PAYLOAD_LENGTH);
}

int RadioDataSent (void)
{
	uint8_t status = RadioGetStatus();
	return status & (1<<5);
}

int RadioDataReceived (void)
{
	uint8_t status = RadioGetStatus();
	return status & (1<<6);
}
