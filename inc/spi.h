/*
 * spi.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef SPI_H_
#define SPI_H_

#include <inttypes.h>

void SPIInit (void);
void SPISend (uint8_t *, uint8_t *, uint8_t, uint8_t);

#endif /* SPI_H_ */
