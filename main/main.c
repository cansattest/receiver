/*
 * main.c
 *
 *  Created on: 20.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>
#include <timers.h>
#include <nrf.h>
#include <util/delay.h>
#include <spi.h>

int main (void)
{
	uint8_t data[PAYLOAD_LENGTH];
	uint8_t extra_data[2] = {0, '\n'};
	USARTInit();
	SPIInit();
	TIM1Init();
	uint8_t ans_flag;
	_delay_ms(200);
	RadioConfig(receive);
	while (1){
		if (RadioDataReceived()){
			RadioSetRegVal (STATUS, 0x40);
			RadioReadData (data);
			USARTSend (data, PAYLOAD_LENGTH);
		}
	}
	return 0;
}

